<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;
use Infotechnohelp\Scope\Traits\Index;

/**
 * Class AssociativeScopeWithIndex
 * @package Infotechnohelp\Scope
 */
class AssociativeScopeWithIndex extends AssociativeScope implements IndexedScopeItem
{
    use Index;
}
