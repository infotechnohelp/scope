<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\AssociativeScopeItem;
use Infotechnohelp\Scope\Traits\Key;

/**
 * Class AssociativeScopeWithKey
 * @package Infotechnohelp\Scope
 */
class AssociativeScopeWithKey extends AssociativeScope implements AssociativeScopeItem
{
    use Key;
}
