<?php

namespace Infotechnohelp\Scope;

/**
 * Class ScopeUnit
 * @package Infotechnohelp\Scope
 */
class ScopeUnit
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function __toString()
    {
        return (string)$this->value;
    }
}
