<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;
use Infotechnohelp\Scope\Traits\Index;

/**
 * Class ScopeUnitWithIndex
 * @package Infotechnohelp\Scope
 */
class ScopeUnitWithIndex extends ScopeUnit implements IndexedScopeItem
{
    use Index;
}
