<?php

namespace Infotechnohelp\Scope;

/**
 * Class Scope
 * @package Infotechnohelp\Scope
 */
class Scope
{
    /**
     * @param array $array
     *
     * @return bool
     */
    private function isAssociativeArray(array $array)
    {
        return array_values($array) !== $array;
    }

    /**
     * @param array $array
     *
     * @return bool
     */
    private function isIndexedArray(array $array)
    {
        return array_values($array) === $array;
    }

    /**
     * @param array $array
     * @param bool  $associative
     *
     * @return Scope
     * @throws \Exception
     */
    public function initFromArray(array $array, bool $associative)
    {
        if ($associative && $this->isIndexedArray($array)) {
            throw new \Exception("Array should be associative");
        }

        if (! $associative && $this->isAssociativeArray($array)) {
            throw new \Exception("Array should be indexed");
        }

        if ($associative) {
            /** @var AssociativeScope $this */
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    if ($this->isIndexedArray($value)) {
                        $this->addScope(
                            $key,
                            (new IndexedScopeWithKey())->initFromArray($value, false)
                        );
                    }

                    if ($this->isAssociativeArray($value)) {
                        $this->addScope(
                            $key,
                            (new AssociativeScopeWithKey())->initFromArray($value, true)
                        );
                    }

                    continue;
                }

                $this->addUnit($key, $value);
            }
        }


        if (! $associative) {
            /** @var IndexedScope $this */
            foreach ($array as $item) {
                if (is_array($item)) {
                    if ($this->isIndexedArray($item)) {
                        $this->addScope((new IndexedScopeWithIndex())->initFromArray($item, false));
                    }

                    if ($this->isAssociativeArray($item)) {
                        $this->addScope((new AssociativeScopeWithIndex())->initFromArray($item, true));
                    }

                    continue;
                }

                $this->addUnit($item);
            }
        }


        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $result = [];

        if ($this instanceof IndexedScope) {
            foreach ($this as $item) {
                if ($item instanceof Scope) {
                    /** @var Scope $item */
                    $result[] = $item->toArray();
                    continue;
                }
                /** @var ScopeUnit $item */
                $result[] = $item->getValue();
            }
        }

        if ($this instanceof AssociativeScope) {
            foreach ($this as $key => $value) {
                if ($value instanceof Scope) {
                    /** @var Scope $value */
                    $result[$key] = $value->toArray();
                    continue;
                }
                /** @var ScopeUnit $value */
                $result[$key] = $value->getValue();
            }
        }

        return $result;
    }
}
