<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\AssociativeScopeItem;
use Infotechnohelp\Scope\Traits\Key;

/**
 * Class IndexedScopeWithKey
 * @package Infotechnohelp\Scope
 */
class IndexedScopeWithKey extends IndexedScope implements AssociativeScopeItem
{
    use Key;
}
