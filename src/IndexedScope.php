<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;

/**
 * Class IndexedScope
 * @package Infotechnohelp\Scope
 */
class IndexedScope extends Scope
{
    /**
     * @var int
     */
    private $i = 0;

    /**
     * IndexedScope constructor.
     *
     * @param array|null $array
     */
    public function __construct(array $array = null)
    {
        if ($array !== null) {
            $this->initFromArray($array);
        }
    }

    /**
     * @param array $array
     * @param bool  $associative
     *
     * @return Scope|IndexedScopeItem
     * @throws \Exception
     */
    public function initFromArray(array $array = [], bool $associative = false)
    {
        if ($associative) {
            throw new \Exception("Array should be indexed");
        }

        return parent::initFromArray($array, $associative);
    }

    /**
     * @param int $i
     *
     * @return mixed
     */
    public function get(int $i): IndexedScopeItem
    {
        return $this->$i;
    }

    /**
     * @return \Infotechnohelp\Scope\Interfaces\IndexedScopeItem
     */
    public function first(): IndexedScopeItem
    {
        if ($this->i === 0) {
            return null;
        }

        return $this->{0};
    }

    /**
     * @return \Infotechnohelp\Scope\Interfaces\IndexedScopeItem
     */
    public function last(): IndexedScopeItem
    {
        return $this->{$this->i - 1};
    }

    /**
     * @param IndexedScopeItem $value
     *
     * @return IndexedScope
     */
    public function addScope(IndexedScopeItem $value)
    {
        /** @var IndexedScopeItem|IndexedScopeWithIndex|AssociativeScopeWithIndex $value */
        $this->{$this->i} = $value->setIndex($this->i)->setParentLength($this->i);

        $properties = (array)$this;

        /** @var  IndexedScopeItem $value */
        foreach ($properties as $key => $value) {
            if (strpos($key, "Infotechnohelp\\Scope") === 1) {
                continue;
            }

            $value->increaseParentLength();
        }

        $this->i++;

        return $this;
    }

    public function addUnit($value)
    {
        $this->{$this->i} = (new ScopeUnitWithIndex($value))->setIndex($this->i)->setParentLength($this->i);

        $properties = (array)$this;

        /** @var IndexedScopeItem $value */
        foreach ($properties as $key => $value) {
            if (strpos($key, "Infotechnohelp\\Scope") === 1) {
                continue;
            }

            $value->increaseParentLength();
        }

        $this->i++;

        return $this;
    }
}
