<?php

namespace Infotechnohelp\Scope\Interfaces;

interface IndexedScopeItem
{
    public function setIndex(int $i);

    public function getIndex(): int;

    public function setParentLength(int $i);

    public function getParentLength(): int;

    public function increaseParentLength(int $i = 1);

    public function isFirst();

    public function isLast();
}
