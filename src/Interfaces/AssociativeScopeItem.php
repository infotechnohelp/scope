<?php

namespace Infotechnohelp\Scope\Interfaces;

interface AssociativeScopeItem
{
    public function setKey(string $key);

    public function getKey();
}
