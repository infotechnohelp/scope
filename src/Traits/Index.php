<?php

namespace Infotechnohelp\Scope\Traits;

/**
 * Trait Index
 * @package Infotechnohelp\Scope\Traits
 */
trait Index
{
    /**
     * @var int|null
     */
    private $index = null;
    /**
     * @var int|null
     */
    private $parentLength = null;

    public function setIndex(int $i): self
    {
        $this->index = $i;

        return $this;
    }

    public function getIndex(): int
    {
        return $this->index;
    }

    public function setParentLength(int $i): self
    {
        $this->parentLength = $i;

        return $this;
    }

    public function getParentLength(): int
    {
        return $this->parentLength;
    }

    public function increaseParentLength(int $i = 1): self
    {
        $this->parentLength = ($this->parentLength === null) ? 1 : $this->parentLength + $i;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isFirst()
    {
        if ($this->index === null) {
            return null;
        }

        return $this->index === 0;
    }

    /**
     * @return bool|null
     */
    public function isLast()
    {
        if ($this->index === null && $this->parentLength === null) {
            return null;
        }

        return $this->index === $this->parentLength - 1;
    }
}
