<?php

namespace Infotechnohelp\Scope\Traits;

/**
 * Trait Key
 * @package Infotechnohelp\Scope\Traits
 */
trait Key
{
    private $key = null;

    public function setKey(string $key): self
    {
        $this->key = $key;

        return $this;
    }

    public function getKey(): string
    {
        return $this->key;
    }
}
