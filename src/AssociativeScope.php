<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\AssociativeScopeItem;
use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;

/**
 * Class AssociativeScope
 * @package Infotechnohelp\Scope
 */
class AssociativeScope extends Scope
{
    /**
     * AssociativeScope constructor.
     *
     * @param array|null $array
     */
    public function __construct(array $array = null)
    {
        if ($array !== null) {
            $this->initFromArray($array);
        }
    }

    /**
     * @param array $array
     * @param bool  $associative
     *
     * @return Scope|IndexedScopeItem
     * @throws \Exception
     */
    public function initFromArray(array $array = [], bool $associative = true)
    {
        if (! $associative) {
            throw new \Exception("Array should be associative");
        }

        return parent::initFromArray($array, $associative);
    }

    /**
     * @param string                      $title
     * @param \Infotechnohelp\Scope\Scope $value
     *
     * @return $this
     */
    public function addScope(string $title, Scope $value)
    {
        /** @var AssociativeScopeItem $value */
        $this->$title = $value->setKey($title);

        return $this;
    }

    /**
     * @param string $title
     * @param        $value
     *
     * @return $this
     */
    public function addUnit(string $title, $value)
    {
        $this->$title = (new ScopeUnitWithKey($value))->setKey($title);

        return $this;
    }

    /**
     * @param string $title
     *
     * @return AssociativeScopeItem|AssociativeScopeWithKey|IndexedScopeWithKey|ScopeUnitWithKey
     */
    public function get(string $title)
    {
        return $this->$title;
    }
}
