<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;
use Infotechnohelp\Scope\Traits\Index;

/**
 * Class IndexedScopeWithIndex
 * @package Infotechnohelp\Scope
 */
class IndexedScopeWithIndex extends IndexedScope implements IndexedScopeItem
{
    use Index;
}
