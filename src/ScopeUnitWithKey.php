<?php

namespace Infotechnohelp\Scope;

use Infotechnohelp\Scope\Interfaces\AssociativeScopeItem;
use Infotechnohelp\Scope\Traits\Key;

/**
 * Class ScopeUnitWithKey
 * @package Infotechnohelp\Scope
 */
class ScopeUnitWithKey extends ScopeUnit implements AssociativeScopeItem
{
    use Key;
}
