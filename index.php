<?php










$fields = (new IndexedScope())->addUnit('title')->addUnit('title 2')->addUnit('title 3');


/*
$names = (new IndexedScope())->addUnit('Philipp')->addUnit('Natasha');

$scope = (new AssociativeScope())->add('fields', $fields)->add('names', $names);
*/
$result = '';

/** @var \ScopeUnitWithIndex $field */
foreach ($fields as $field) {

    $str = '';

    if ($field->isFirst()) {
        $str = 'First: ' . $field;
    }

    if ($field->isLast()) {
        $str = 'Last: ' . $field;
    }

    $result .= $str;

}

echo $result;


/*
foreach($scope->getByKey('fields') as $field){
    if($field->isFirst()){
        echo 'First:' . $field;
    }

    if($field->isLast()){
        echo 'Last:' . $field;
    }
}
*/

/*
foreach($scope->getByKey('fields') as $field){
    echo $field;
    // if($field->isLast()){}
}

foreach($scope as $group){

    foreach($group as $element){
        echo $element;
    }

}
*/