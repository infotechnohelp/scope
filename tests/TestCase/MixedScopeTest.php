<?php

namespace Infotechnohelp\Scope\Test\TestCase;

use Infotechnohelp\Scope\AssociativeScope;
use Infotechnohelp\Scope\AssociativeScopeWithIndex;
use Infotechnohelp\Scope\IndexedScopeWithKey;
use PHPUnit\Framework\TestCase;

/**
 * Class MixedScopeTest
 * @package Infotechnohelp\Scope\Test\TestCase
 */
class MixedScopeTest extends TestCase
{
    public function testMixed()
    {
        $scope = (new AssociativeScope())
        ->addUnit('tableTitle', 'Users')
        ->addScope(
            'fields',
            (new IndexedScopeWithKey())
                ->addScope(
                    (new AssociativeScopeWithIndex())
                        ->addUnit('title', 'username')
                        ->addUnit('default', null)
                        ->addUnit('type', 'string')
                )
                ->addScope(
                    (new AssociativeScopeWithIndex())
                        ->addUnit('title', 'port')
                        ->addUnit('default', 'null')
                        ->addUnit('type', 'integer')
                )
        );

        $expected = "public function create(string \$username, int \$port = null): Users\n";
        $expected .= "{\n";
        $expected .= "}";


        $result = '';

        $result .= "public function create(";
        /** @var AssociativeScopeWithIndex $field */
        foreach ($scope->get('fields') as $field) {
            if (!$field->isFirst()) {
                $result .= ", ";
            }

            $type = $field->get('type');
            if ($type == 'integer') {
                $type = 'int';
            }

            $result .= $type . ' $' . $field->get('title');

            if ($field->get('default')->getValue() !== null) {
                $result .= " = " . $field->get('default');
            }
        }

        $result .= "): " . $scope->get('tableTitle') . "\n";

        $result .= "{\n";
        $result .= "}";


        $this->assertEquals($expected, $result);
    }
}
