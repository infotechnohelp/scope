<?php

namespace Infotechnohelp\Scope\Test\TestCase;

use Infotechnohelp\Scope\IndexedScope;
use Infotechnohelp\Scope\IndexedScopeWithIndex;
use Infotechnohelp\Scope\Interfaces\IndexedScopeItem;
use Infotechnohelp\Scope\Scope;
use Infotechnohelp\Scope\ScopeUnit;
use PHPUnit\Framework\TestCase;

/**
 * Class IndexedScopeWithIndexTest
 * @package PackageSkeleton\Test\TestCase
 */
class IndexedScopeTest extends TestCase
{
    public function testInstanceOf()
    {
        $scope = new IndexedScopeWithIndex();

        $this->assertInstanceOf(Scope::class, $scope);
        $this->assertInstanceOf(IndexedScope::class, $scope);
        $this->assertInstanceOf(IndexedScopeWithIndex::class, $scope);

        $this->assertInstanceOf(IndexedScopeItem::class, $scope);
    }

    public function testAddUnit_integer()
    {
        $scope = (new IndexedScope())->addUnit(1)->addUnit(2);
        /** @var \Infotechnohelp\Scope\ScopeUnitWithIndex $item */
        foreach ($scope as $item) {
            $this->assertFalse(is_int($item));
            $this->assertTrue(is_string((string)$item));
            $this->assertTrue(is_int((int)(string)$item));
            $this->assertTrue(is_int($item->getValue()));
        }

        $first = $scope->first();
        $this->assertTrue($first->isFirst());
        /** @var \Infotechnohelp\Scope\ScopeUnitWithIndex $first */
        $this->assertEquals(1, $first->getValue());

        $last = $scope->last();
        $this->assertTrue($last->isLast());
        /** @var \Infotechnohelp\Scope\ScopeUnitWithIndex $last */
        $this->assertEquals(2, $last->getValue());
    }

    public function testAddUnit_string()
    {
        $scope = (new IndexedScope())->addUnit('Philipp');
        /** @var \Infotechnohelp\Scope\ScopeUnitWithIndex $item */
        foreach ($scope as $item) {
            $this->assertFalse(is_string($item));
            $this->assertTrue(is_string((string)$item));
            $this->assertTrue(is_string($item->getValue()));
        }

        $this->assertEquals($scope->first(), $scope->last());

        $first = $scope->first();
        $this->assertEquals('Philipp', $first);
        $this->assertTrue('Philipp' == $first);
        $this->assertTrue('Philipp' === (string)$first);
        $this->assertFalse('Philipp' === $first);

        /** @var \Infotechnohelp\Scope\ScopeUnitWithIndex $first */
        $this->assertEquals('Philipp', $first->getValue());
        $this->assertTrue('Philipp' === $first->getValue());
    }

    public function testGet()
    {
        $scope = (new IndexedScope())->addUnit('Philipp')->addUnit('Nikolajev')->addUnit(1992);

        $this->assertTrue($scope->first() === $scope->get(0));
        $this->assertTrue($scope->last() === $scope->get(2));
        $this->assertTrue('Nikolajev' === $scope->get(1)->getValue());
    }

    public function testIsFirst()
    {
        $scope = new IndexedScopeWithIndex();

        $this->assertNull($scope->isFirst());
    }

    public function testIsLast()
    {
        $scope = new IndexedScopeWithIndex();

        $this->assertNull($scope->isLast());
    }

    public function testAddScope()
    {
        $generatedScope = new IndexedScope([
            [1, 2, 3, 4,],
            ['Philipp', 'Nikolajev',],
        ]);

        $scope = (new IndexedScope())
            ->addScope(
                (new IndexedScopeWithIndex())->addUnit(1)->addUnit(2)->addUnit(3)->addUnit(4)
            )->addScope(
                (new IndexedScopeWithIndex())->addUnit('Philipp')->addUnit('Nikolajev')
            );

        $this->assertEquals($generatedScope, $scope);


        $this->assertTrue($scope->first()->isFirst());
        $this->assertTrue($scope->last()->isLast());


        $expected = '1+2+3+4; Philipp+Nikolajev';

        $result = '';
        /** @var \Infotechnohelp\Scope\ScopeUnitWithIndex $number */
        foreach ($scope->first() as $number) {
            if ($number->isFirst()) {
                $result .= $number;
                continue;
            }

            if ($number->isLast()) {
                $result .= '+' . $number . ';';
                continue;
            }

            $result .= '+' . $number;
        }
        /** @var \Infotechnohelp\Scope\ScopeUnitWithIndex $name */
        foreach ($scope->last() as $name) {
            if ($name->isFirst()) {
                $result .= ' ' . $name;
                continue;
            }

            $result .= '+' . $name;
        }

        $this->assertEquals($expected, $result);
    }

    public function testAddMixed()
    {
        $generatedScope = new IndexedScope([
            'Unit1',
            'Unit2',
            ['ScopeUnit1', 'ScopeUnit2',],
            'Unit3',
        ]);

        $scope = (new IndexedScope())
            ->addUnit('Unit1')
            ->addUnit('Unit2')
            ->addScope((new IndexedScopeWithIndex())->addUnit('ScopeUnit1')->addUnit('ScopeUnit2'))
            ->addUnit('Unit3');

        $this->assertEquals($generatedScope, $scope);


        $expected = 'Unit1Unit2Unit3ScopeUnit1ScopeUnit2';

        $result = '';

        foreach ($scope as $item) {
            if ($item instanceof Scope) {
                continue;
            }

            $result .= $item;
        }

        foreach ($scope as $item) {
            if ($item instanceof ScopeUnit) {
                continue;
            }

            foreach ($item as $subItem) {
                $result .= $subItem;
            }
        }

        $this->assertEquals($expected, $result);
    }
}
