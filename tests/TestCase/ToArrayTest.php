<?php

namespace Infotechnohelp\Scope\Test\TestCase;

use Infotechnohelp\Scope\AssociativeScope;
use Infotechnohelp\Scope\IndexedScope;
use PHPUnit\Framework\TestCase;

/**
 * Class ToArrayTest
 * @package Infotechnohelp\Scope\Test\TestCase
 */
class ToArrayTest extends TestCase
{
    public function testToArray()
    {
        $array = [1, 2, 3,];

        $scope = new IndexedScope($array);

        $this->assertEquals($array, $scope->toArray());

        $array = [
            'tableTitle' => 'Users',
            'fields'     => [
                [
                    'title'   => 'username',
                    'default' => null,
                    'type'    => 'string',
                ],
                [
                    'title'   => 'port',
                    'default' => 'null',
                    'type'    => 'integer',
                ],
            ],
        ];

        $scope = new AssociativeScope($array);

        $this->assertEquals($array, $scope->toArray());
    }
}
