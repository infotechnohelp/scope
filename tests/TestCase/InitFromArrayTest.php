<?php

namespace Infotechnohelp\Scope\Test\TestCase;

use Infotechnohelp\Scope\AssociativeScope;
use Infotechnohelp\Scope\AssociativeScopeWithIndex;
use Infotechnohelp\Scope\IndexedScope;
use Infotechnohelp\Scope\IndexedScopeWithKey;
use PHPUnit\Framework\TestCase;

/**
 * Class InitFromArrayTest
 * @package Infotechnohelp\Scope\Test\TestCase
 */
class InitFromArrayTest extends TestCase
{
    public function testInitFromArray()
    {
        $array = [
            'title'  => 'Users',
            'fields' => [1, 2, 3,],
        ];
        /** @var AssociativeScope $scope */
        $scope = (new AssociativeScope())->initFromArray($array);

        $this->assertEquals('Users', $scope->get('title'));

        /** @var \Infotechnohelp\Scope\ScopeUnit $last */
        $last = $scope->get('fields')->last();
        $this->assertEquals(3, $last->getValue());


        $array = [
            'Users',
            [
                'title' => 'TableTitle',
                'type'  => 'Table',
            ],
        ];

        /** @var IndexedScope $scope */
        $scope = (new IndexedScope())->initFromArray($array);

        $this->assertEquals('Users', $scope->first());

        /** @var AssociativeScope $last */
        $last = $scope->last();
        $this->assertEquals('Table', $last->get('type'));
    }

    public function testInitFromLargeArray()
    {
        $array = [
            'tableTitle' => 'Users',
            'fields'     => [
                [
                    'title'   => 'username',
                    'default' => null,
                    'type'    => 'string',
                ],
                [
                    'title'   => 'port',
                    'default' => 'null',
                    'type'    => 'integer',
                ],
            ],
        ];

        $generatedScope = (new AssociativeScope())->initFromArray($array);

        $scope = (new AssociativeScope())
            ->addUnit('tableTitle', 'Users')
            ->addScope(
                'fields',
                (new IndexedScopeWithKey())
                    ->addScope(
                        (new AssociativeScopeWithIndex())
                            ->addUnit('title', 'username')
                            ->addUnit('default', null)
                            ->addUnit('type', 'string')
                    )
                    ->addScope(
                        (new AssociativeScopeWithIndex())
                            ->addUnit('title', 'port')
                            ->addUnit('default', 'null')
                            ->addUnit('type', 'integer')
                    )
            );

        $this->assertEquals($generatedScope, $scope);
    }
}
