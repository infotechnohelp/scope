<?php

namespace Infotechnohelp\Scope\Test\TestCase;

use Infotechnohelp\Scope\AssociativeScope;
use Infotechnohelp\Scope\AssociativeScopeWithKey;
use PHPUnit\Framework\TestCase;

/**
 * Class AssociativeScopeTest
 * @package Infotechnohelp\Scope\Test\TestCase
 */
class AssociativeScopeTest extends TestCase
{
    public function testAddUnit()
    {
        $generatedScope = new AssociativeScope([
            'firstName'  => 'Philipp',
            'secondName' => 'Nikolajev',
        ]);

        $scope = (new AssociativeScope())->addUnit('firstName', 'Philipp')->addUnit('secondName', 'Nikolajev');

        $this->assertEquals($generatedScope, $scope);


        $this->assertTrue($scope->get('firstName')->getKey() === 'firstName');

        $expected = 'Philipp Nikolajev';

        $result = $scope->get('firstName') . ' ' . $scope->get('secondName');

        $this->assertEquals($expected, $result);
    }

    public function testAddScope()
    {
        $generatedScope = new AssociativeScope([
            'fields' => [
                'title' => 'Field',
            ],
        ]);

        $scope = (new AssociativeScope())->addScope(
            'fields',
            (new AssociativeScopeWithKey())->addUnit('title', 'Field')
        );

        $this->assertEquals($generatedScope, $scope);


        $this->assertTrue($scope->get('fields')->getKey() === 'fields');

        $this->assertEquals('Field', $scope->get('fields')->get('title'));
    }
}
